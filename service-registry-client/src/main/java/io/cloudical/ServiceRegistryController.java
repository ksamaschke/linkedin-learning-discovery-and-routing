package io.cloudical;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceRegistryController {

	@Autowired
	private DiscoveryClient discoveryClient;
	
	@RequestMapping("/instances/{appName}")
	public List<ServiceInstance> getInstancesForApplication(
			@PathVariable String appName) {
		
		return discoveryClient.getInstances(appName);
		
	}
	
}
